package com.manojdeo.cloud.resource;

import org.apache.commons.lang.math.RandomUtils;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController("/rest")
public class RestResource {
	
	@HystrixCommand(fallbackMethod = "fallBackHello",commandKey = "hello",groupKey = "hello")
	@GetMapping("/hello")
	public String hello() {
		if(RandomUtils.nextBoolean()) {
			throw new RuntimeException("Failed");
		}
		
		return "hello";
	}
	
	public String fallBackHello() {
		return  "fall abck Hello initiated";
	}

}
